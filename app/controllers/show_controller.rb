class ShowController < ApplicationController
  def index
    get_vm(params[:id])
    @cf_node = Rails.configuration.application['NETBOX_NODE_CF']
    @cf_roles = Rails.configuration.application['NETBOX_ROLES_CF']
    @cf_vmid = Rails.configuration.application['NETBOX_PVEVMID_CF']
    @mmonit_enabled = Rails.configuration.application['MMONIT_INTEGRATION']
    @netbox_url = Rails.configuration.application['NETBOX_URL']
    @netdata_url = Rails.configuration.application['NETDATA_URL']

  end


  def destroy
    id = params[:id]
    redirect_to :controller => 'welcome', :action => 'index'
    # Set configuration for delete
    begin
      NetboxClientRuby.configure do |c|
        c.netbox.auth.token = current_user.netbox_token    
        c.netbox.api_base_url = Rails.configuration.application['NETBOX_API_URL']
      end
      s = NetboxClientRuby.virtualization.virtual_machine(id)
      name = s.name
      s.delete  
    rescue => exception
      flash[:error] = "Unable to delete VM on Netbox."
    end
    begin
      rundeck_url = Rails.configuration.application['RUNDECK_URL']
      rundeck_job_id = Rails.configuration.application['RUNDECK_TERRAFORMGEN_JOBID']
      require 'rundeck'
      options = { :endpoint => rundeck_url, :api_token => current_user.rundeck_token }
      rd = Rundeck::Client.new(options)
      ssl_verify = {:verify => false}
      out = rd.execute_job(rundeck_job_id)        
    rescue => exception
      flash[:error] = "Unable to push changes on Rundeck."
    end
    begin
      delete_mmonit_for_vm(name)
    rescue => exception
      puts "Error call mmonit delete"
    end
    flash[:deleted] = "VM " + params[:id] + " (" + name + ") supprimée."
    msgText = "VM " + params[:id] + " (" + name + ") deleted."
    @note = Note.new(title: "VM deleted", text: msgText, author: current_user.email)
    @note.save   
  end

  def stop
    id = params[:id]
  end






end
