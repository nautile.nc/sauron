class User < ApplicationRecord

  include Gravtastic
  gravtastic
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  #devise :ldap_authenticatable, :registerable,
  devise :ldap_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  validates :username, presence: true, uniqueness: true


  before_validation :get_ldap_email
  def get_ldap_email
        begin
          email = Devise::LDAP::Adapter.get_ldap_param(self.username,"mail").first
        rescue => exception
          email = "no-mail-in-ldap@nautile.nc"
        end
  	self.email = email
  end

  before_validation :get_ldap_id
  def get_ldap_id
  	self.id = Devise::LDAP::Adapter.get_ldap_param(self.username,"uidnumber").first
  end
  def authenticatable_salt
  	Digest::SHA1.hexdigest(email)[0,29]
  end

  #def email_required?
  #  false
  #end

  #attr_writer :login
  #def login
  #  @login || self.username || self.email
  #end


#  def update_with_password(params)
#    @new_password = params[:password]
#    @current_password = params[:current_password]
#  end

  #def update_with_password(params={}) 
  def update_with_password(params) 
    #@current_password = params[:current_password]
    #@new_password = params[:password]
    if params[:password].blank? 
      params.delete(:password) 
      params.delete(:password_confirmation) if params[:password_confirmation].blank? 
    end 
    update(params) 
  end

end
